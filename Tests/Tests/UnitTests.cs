using System;
using Xunit;
using DataLayer;
using System.Collections.Generic;

namespace Tests
{
    public class UnitTests
    {

        static List<TankCommandData>  tanks = new List<TankCommandData>() {
                new TankCommandData{ user = "user1", name="tank1", command="move",  data="10, 10", isGoodData = true },
                new TankCommandData{ user = "user2", name="tank1", command="rotate", data="3, 6, 12", isGoodData = false},
                new TankCommandData{ user = "user3", name="tank2", command="shoot", data="45, 343, 67", isGoodData = true}
        };


        [Fact]
        public void TestEngine_BreackAllSimulation_Throws()
        {
            DataEngine engine = new DataEngine();
            
            engine.tanks = tanks;

            Assert.Throws<Exception>(() => engine.Start_BreackAllSimulation_Throws());
        }


        [Fact]
        public void TestEngine_CenceledProcessSimulation_Throws()
        {
            DataEngine engine = new DataEngine();

            engine.tanks = tanks;

            Assert.Throws<Exception>( () => engine.Start_CenceledProcessSimulation_Throws() );
        }


        [Fact]
        public void TestEngine_NullRefenceSimulation_Throws()
        {

            DataEngine engine = new DataEngine();

            engine.tanks = tanks;

            Assert.Throws<Exception>(() => engine.Start_NullRefenceSimulation_Throws());
        }


        [Fact]
        public void TestEngine_NullRefenceSimulation_NotThrows()
        {

            DataEngine engine = new DataEngine();

            engine.tanks = tanks;

            //Act
            var exception = Record.Exception(() => engine.Start_NullRefenceSimulation_NotThrows());

            //Assert
            Assert.Null(exception);


        }


    }
}
