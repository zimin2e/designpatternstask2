﻿using System.Threading;

namespace DataLayer
{
    class RotateCommand
    {
        public string Execute(TankCommandData data, CancellationToken token)
        {

            SomeWork(token);

            return data.ToString();
        }

        void SomeWork(CancellationToken cancellationToken)
        {
            int result;
            int i = 0;
            while (i < 10000)
            {
                // Что-то делаем ...
                // ... и периодически проверяем, не запрошена ли отмена операции
                cancellationToken.ThrowIfCancellationRequested();
                i++;
            }
        }
    }

}
