﻿using System.Runtime.CompilerServices;
using System;
[assembly: InternalsVisibleTo("Tests")]
namespace DataLayer
{
    class TankCommandData {
        internal string user;
        internal string name;
        internal string command;
        internal string data;
        internal bool isGoodData;

        internal string ToString()
        {
            if (data == null) throw new ArgumentException();
            return user + " " + name + " " + command + " " + data + " " + isGoodData;
        }



    }
}
