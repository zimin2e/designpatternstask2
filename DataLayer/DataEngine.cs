﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("Tests")]
namespace DataLayer
{
    class DataEngine
    {
        public List<TankCommandData> tanks = new List<TankCommandData>();





        public void Start_BreackAllSimulation_Throws()
        {
            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
            CancellationToken token = cancelTokenSource.Token;
            try
            {
                foreach (var tank in tanks)
                {

                    switch (tank.command)
                    {
                        case "move":
                            {
                                //CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
                                //CancellationToken token = cancelTokenSource.Token;

                                Task.Run(
                                    () => new MoveCommand().Execute(tank, token)
                                    , token
                                ).GetAwaiter().GetResult();
                                break;
                            }
                        case "rotate":
                            {


                                tank.data = null;

                                Task.Run(
                                    () => new RotateCommand().Execute(null, token)
                                    , token
                                ).GetAwaiter().GetResult();
                                break;
                            }//
                        case "shoot":
                            {
                                try
                                {
                                    Task.Run(
                                        () => new ShootCommand().Execute(tank, token)
                                        , token
                                    ).GetAwaiter().GetResult(); 

                                }
                                catch (OperationCanceledException e)
                                {
                                    new Exception("Shoot canceled");
                                }
                                break;
                            }
                    }

                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }






        public void Start_CenceledProcessSimulation_Throws()
        {
            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();

                CancellationToken token = cancelTokenSource.Token;
                foreach (var tank in tanks)
                {
                try
                {
                    switch (tank.command)
                    {
                        case "move":
                            {
                                Task.Run(
                                    () => new MoveCommand().Execute(tank, token)
                                    , token
                                ).GetAwaiter().GetResult();
                                break;
                            }
                        case "rotate":
                            {
                                Task.Run(
                                    () => new RotateCommand().Execute(null, token)
                                    , token
                                ).GetAwaiter().GetResult();
                                break;
                            }
                        case "shoot":
                            {
                                try
                                {
                                    cancelTokenSource.Cancel();
                         
                                    Task.Run(
                                        () => new ShootCommand().Execute(tank, token)
                                        , token
                                    ).GetAwaiter().GetResult();

                                }
                                catch (OperationCanceledException e)
                                {
                                    new Exception("Shoot canceled");
                                }
                            break;
                        }
                       
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            } // for                             


        }





        public void Start_NullRefenceSimulation_Throws()
        {
            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
            CancellationToken token = cancelTokenSource.Token;
            foreach (var tank in tanks)
            {
                try
                {
                    switch (tank.command)
                    {
                        case "move":
                            {
                                Task.Run(
                                    () => new MoveCommand().Execute(tank, token)
                                    , token
                                ).GetAwaiter().GetResult();
                                break;
                            }
                        case "rotate":
                            {

                                /* Set null to throw excetion*/
                                Task.Run(
                                    () => new RotateCommand().Execute(null, token)
                                    , token
                                ).GetAwaiter().GetResult();
                                break;
                            }//
                        case "shoot":
                            {
                                try
                                {
                                    /* Cancel to throw Exception */
                                    cancelTokenSource.Cancel();

                                    Task.Run(
                                        () => new ShootCommand().Execute(tank, token)
                                        , token
                                    ).GetAwaiter().GetResult();

                                }
                                catch (OperationCanceledException e)
                                {
                                    new Exception("Shoot canceled");
                                }
                                break;
                            }

                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            } // for                             


        }




        public void Start_NullRefenceSimulation_NotThrows()
        {
            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();


            CancellationToken token = cancelTokenSource.Token;
            foreach (var tank in tanks)
            {
                try
                {
                    switch (tank.command)
                    {
                        case "move":
                            {

                                Task.Run(
                                    () => new MoveCommand().Execute(tank, token)
                                    , token
                                ).GetAwaiter().GetResult();
                                break;
                            }
                        case "rotate":
                            {
                                /*******************************/
                                /*   Catch Exception in thread */
                                /*******************************/
                                try
                                {
                                    /* Set null to throw excetion*/
                                    Task.Run(
                                        () => new RotateCommand().Execute(null, token)
                                        , token
                                    ).GetAwaiter().GetResult();
                                }catch (Exception e) { }
                                break;
                            }//
                        case "shoot":
                            {
                                try
                                {
                                    /* Cancel to throw Exception */

                                    cancelTokenSource.Cancel();

                                    Task.Run(
                                        () => new ShootCommand().Execute(tank, token)
                                        , token
                                    ).GetAwaiter().GetResult();

                                }
                                catch (OperationCanceledException e)
                                {
                                    new Exception("Shoot canceled");
                                }
                                break;
                            }

                    }
                }
                catch (Exception e) 
                {
                    throw new Exception(e.Message);
                }
            } // for                             


        }


    }
}
