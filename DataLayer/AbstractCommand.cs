﻿using System.Threading;

namespace DataLayer
{
    abstract class AbstractCommand
    {
        public abstract string Execute(TankCommandData data, CancellationToken token);
    }

}
